# Sharepoint API Wrapper

Sharepoint API Wrapper is a simple library used to interact with a Sharepoint site.

## How to use it ?

### Installation

First of all, you need to install the library.

```
npm install sharepoint-api-wrapper
```

### Importing

After installation of the library, you need to import it in your project.

For ESModules :

```
import { SharepointApi } from 'sharepoint-api-wrapper';
```

For CommonJS :

```
const { SharepointApi } = require('sharepoint-api-wrapper');
```

### Configuration

```
const options = {
	authOptions: {
		username: 'username@outlook.com',
		password: 'MyPassword',
	},
	url: 'https://my-organization.sharepoint.com',
	siteName: 'NameOfMySite',
	baseFolder: 'Shared documents', // This is optionnal
};

const api = new SharepointApi(options);
```

All options are interfaced with `SharepointApiOptions`.

|   parameter   |      type      | required |                                   description                                    |
| :-----------: | :------------: | :------: | :------------------------------------------------------------------------------: |
| `authOptions` | `IAuthOptions` |   true   | `IAuthOptions` from [`node-sp-auth`](https://www.npmjs.com/package/node-sp-auth) |
|     `url`     |    `string`    |   true   |                                Url of Sharepoint                                 |
|  `siteName`   |    `string`    |   true   |                            Name of Sharepoint site\*                             |
| `baseFolder`  |    `string`    |  false   |                   Used to make it root folder for api wrapper                    |

\* To get the name of your Sharepoint site, simply go at your sharepoint from your webbrowser, then go into your Sharepoint site. You can get the name in the URL (ex: `https://sharepoint.com/sites/MySite`, here `MySite` is the siteName)

## Methods

- [.getClientContext()](#getclientcontext)
- [.getSiteInfos()](#getsiteinfos)
- [.getFolders(folderRelativeUrl)](#getfoldersfolderrelativeurl)
- [.getFolder(folderRelativeUrl)](#getfolderfolderrelativeurl)
- [.createFolder(folderName, folderRelativeUrl)](#createfolderfoldername-folderrelativeurl)
- [.deleteFolder(folderRelativeUrl)](#deletefolderfolderrelativeurl)
- [.getFiles(folderRelativeUrl)](#getfilesfolderrelativeurl)
- [.getFile(fileRelativeUrl)](#getfilefilerelativeurl)
- [.downloadFile(fileRelativeUrl)](#downloadfilefilerelativeurl)
- [.uploadFile(fileRelativeUrl)](#uploadfilefilerelativeurl)
- [.moveFile(fileRelativeUrl, folderRelativeUrl)](#movefilefilerelativeurl-folderrelativeurl)
- [.deleteFile(fileRelativeUrl)](#deletefilefilerelativeurl)

### .getClientContext()

Get api client context.

Returns `Promise<SpClientContext | undefined>`

### .getSiteInfos()

Get site informations.

Returns `Promise<SpWeb | undefined>`

### .getFolders(folderRelativeUrl)

List folders from a folder url.

|      parameter      |   type   | required |      description      |
| :-----------------: | :------: | :------: | :-------------------: |
| `folderRelativeUrl` | `string` |  false   | baseFolder by default |

Returns `Promise<SpFolder[] | undefined>`

### .getFolder(folderRelativeUrl)

Get a folder from a folder url.

|      parameter      |   type   | required |      description      |
| :-----------------: | :------: | :------: | :-------------------: |
| `folderRelativeUrl` | `string` |  false   | baseFolder by default |

Returns `Promise<SpFolder | undefined>`

### .createFolder(folderName, folderRelativeUrl)

Create a new folder.

|      parameter      |   type   | required |                  description                   |
| :-----------------: | :------: | :------: | :--------------------------------------------: |
|    `folderName`     | `string` |   true   |            Name of folder to create            |
| `folderRelativeUrl` | `string` |  false   | Where to create folder (baseFolder by default) |

Returns `Promise<SpFolder | undefined>`.

### .deleteFolder(folderRelativeUrl)

Delete an existing folder.

|      parameter      |   type   | required |             description              |
| :-----------------: | :------: | :------: | :----------------------------------: |
| `folderRelativeUrl` | `string` |   true   | The relative url of folder to delete |

Returns `Promise<void>`

### .getFiles(folderRelativeUrl)

Lists files from a folder.

|      parameter      |   type   | required |      description      |
| :-----------------: | :------: | :------: | :-------------------: |
| `folderRelativeUrl` | `string` |  false   | baseFolder by default |

Returns `Promise<SpFile[] | undefined>`

### .getFile(fileRelativeUrl)

Get a file from it's relative url.

|     parameter     |   type   | required |         description         |
| :---------------: | :------: | :------: | :-------------------------: |
| `fileRelativeUrl` | `string` |   true   | Relative url of file to get |

Returns `Promise<SpFile | undefined>`

### .downloadFile(fileRelativeUrl)

Download a file from it's relative url.

|     parameter     |   type   | required |           description            |
| :---------------: | :------: | :------: | :------------------------------: |
| `fileRelativeUrl` | `string` |   true   | Relative url of file to download |

Returns `Promise<SpDownloadFile | undefined>`

### .uploadFile(fileName, file, folderRelativeUrl)

Download a file from it's relative url.

|      parameter      |   type   | required |                description                |
| :-----------------: | :------: | :------: | :---------------------------------------: |
|     `fileName`      | `string` |   true   | Name of the file to upload with extension |
|       `file`        | `Buffer` |   true   |       Content of the file to upload       |
| `folderRelativeUrl` | `string` |   true   |  Relative url of folder where to upload   |

Returns `Promise<SpFile | undefined>`

### .moveFile(fileRelativeUrl, folderRelativeUrl)

Move a file to another folder.

|      parameter      |   type   | required |                  description                  |
| :-----------------: | :------: | :------: | :-------------------------------------------: |
|  `fileRelativeUrl`  | `string` |   true   |       Relative url of the file to move        |
| `folderRelativeUrl` | `string` |   true   | Relative url of folder where to move the file |

Returns `Promise<void>`

### .deleteFile(fileRelativeUrl)

Delete a file.

|     parameter     |   type   | required |            description             |
| :---------------: | :------: | :------: | :--------------------------------: |
| `fileRelativeUrl` | `string` |   true   | Relative url of the file to delete |

Returns `Promise<void>`

## Interfaces

- [SharepointApiOptions](#sharepointapioptions)
- [SpDownloadFile](#spdownloadfile)

### SharepointApiOptions

|   property    |      type      | required |                                   description                                    |
| :-----------: | :------------: | :------: | :------------------------------------------------------------------------------: |
| `authOptions` | `IAuthOptions` |   true   | `IAuthOptions` from [`node-sp-auth`](https://www.npmjs.com/package/node-sp-auth) |
|     `url`     |    `string`    |   true   |                              Url of the Sharepoint                               |
|  `siteName`   |    `string`    |   true   |                            Name of Sharepoint site\*                             |
| `baseFolder`  |    `string`    |  false   |                   Used to make it root folder for api wrapper                    |

\* To get the name of your Sharepoint site, simply go at your sharepoint from your webbrowser, then go into your Sharepoint site. You can get the name in the URL (ex: https://sharepoint.com/sites/MySite, here MySite is the siteName)

### SpDownloadFile

|  property  |   type   |           description           |
| :--------: | :------: | :-----------------------------: |
| `fileName` | `string` | Name of the file with extension |
|  `buffer`  | `Buffer` |       Content of the file       |
