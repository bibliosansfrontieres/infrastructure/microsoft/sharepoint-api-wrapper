import { SharepointApi, SharepointApiOptions } from '../src';
import * as fs from 'fs';
import { join } from 'path';
import dotenv from 'dotenv';
dotenv.config();

const username = process.env.SHAREPOINT_USERNAME || '';
const password = process.env.SHAREPOINT_PASSWORD || '';
const url = process.env.SHAREPOINT_URL || '';
const siteName = process.env.SHAREPOINT_SITE_NAME || '';
const baseFolder = process.env.SHAREPOINT_BASE_FOLDER;

const options: SharepointApiOptions = {
  authOptions: {
    username,
    password,
  },
  url,
  siteName,
  baseFolder,
};
const api: SharepointApi = new SharepointApi(options);

const testFolderName = 'testing_api_wrapper';
const testMovingFolderName = 'testing_moving_folder';
const testRenameFolderName = 'testing_renaming_folder';
const testFileName = 'test.txt';

const fileToUpload = fs.readFileSync(
  join(__dirname, `./files/${testFileName}`),
);

describe('Sharepoint API wrapper', () => {
  test('getClientContext()', async () => {
    const clientContext = await api.getClientContext();
    expect(clientContext).toBeDefined();
    expect(clientContext?.GetContextWebInformation.SiteFullUrl).toBe(
      `${url}/sites/${siteName}`,
    );
  });

  test('getSiteInfos()', async () => {
    const siteInfos = await api.getSiteInfos();
    expect(siteInfos).toBeDefined();
    expect(siteInfos?.ServerRelativeUrl).toBe(`/sites/${siteName}`);
  });

  test('createFolder()', async () => {
    const folder = await api.createFolder(testFolderName);
    expect(folder).toBeDefined();
  });

  test('getFolder()', async () => {
    const folder = await api.getFolder(testFolderName);
    expect(folder).toBeDefined();
    expect(folder?.Name).toBe(testFolderName);
  });

  test('getFolders()', async () => {
    const folders = await api.getFolders();
    expect(folders).toBeDefined();
    if (!folders) return;
    let foundCreatedFolder = false;
    for (const folder of folders) {
      if (folder.Name === testFolderName) {
        foundCreatedFolder = true;
      }
    }
    expect(foundCreatedFolder).toBe(true);
  });

  test('uploadFile()', async () => {
    const file = await api.uploadFile(
      testFileName,
      fileToUpload,
      testFolderName,
    );
    expect(file).toBeDefined();
    expect(file?.Name).toBe(testFileName);
  });

  test('getFile()', async () => {
    const file = await api.getFile(`${testFolderName}/${testFileName}`);
    expect(file).toBeDefined();
    expect(file?.Name).toBe(testFileName);
  });

  test('getFileByUniqueId()', async () => {
    const relativeFile = await api.getFile(`${testFolderName}/${testFileName}`);
    expect(relativeFile).toBeDefined();
    if (!relativeFile) return;
    const file = await api.getFileByUniqueId(relativeFile?.UniqueId);
    expect(file).toBeDefined();
    expect(file?.Name).toBe(testFileName);
  });

  test('getFiles()', async () => {
    const files = await api.getFiles(`${testFolderName}`);
    expect(files).toBeDefined();
    if (!files) return;
    let foundCreatedFile = false;
    for (const file of files) {
      if (file.Name === testFileName) {
        foundCreatedFile = true;
      }
    }
    expect(foundCreatedFile).toBe(true);
  });

  test('downloadFile()', async () => {
    const filePath = join(__dirname, './files/test_dl.txt');
    const writeStream = fs.createWriteStream(filePath);
    const file = await api.downloadFile(
      `${testFolderName}/${testFileName}`,
      writeStream,
    );
    expect(file).toBe(true);
    fs.rmSync(filePath);
  });

  test('downloadFileByUniqueId()', async () => {
    const relativeFile = await api.getFile(`${testFolderName}/${testFileName}`);
    expect(relativeFile).toBeDefined();
    if (!relativeFile) return;
    const filePath = join(__dirname, './files/test_dl2.txt');
    const writeStream = fs.createWriteStream(filePath);
    const file = await api.downloadFileById(relativeFile.UniqueId, writeStream);
    expect(file).toBe(true);
    fs.rmSync(filePath);
  });

  test('moveFile()', async () => {
    await api.moveFile(`${testFolderName}/${testFileName}`, '/');
    const file = await api.getFile(testFileName);
    expect(file).toBeDefined();
    expect(file?.Name).toBe(testFileName);
  });

  test('deleteFile()', async () => {
    await api.deleteFile(testFileName);
    const file = await api.getFile(testFileName);
    expect(file).toBeUndefined();
  });

  test('moveFolder()', async () => {
    await api.createFolder(testMovingFolderName);
    await api.moveFolder(testFolderName, testMovingFolderName);
    const folder = await api.getFolder(
      `/${testMovingFolderName}/${testFolderName}`,
    );
    expect(folder).toBeDefined();
  });

  test('moveFolder() with renaming', async () => {
    await api.moveFolder(
      `/${testMovingFolderName}/${testFolderName}`,
      testMovingFolderName,
      testRenameFolderName,
    );
    const folder = await api.getFolder(
      `/${testMovingFolderName}/${testRenameFolderName}`,
    );
    expect(folder).toBeDefined();
  });

  test('deleteFolder()', async () => {
    await api.deleteFolder(`/${testMovingFolderName}/${testFolderName}`);
    await api.deleteFolder(testMovingFolderName);
    const folder = await api.getFolder(testMovingFolderName);
    expect(folder).toBeUndefined();
  });
});
