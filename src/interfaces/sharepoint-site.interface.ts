import { IAuthOptions } from 'node-sp-auth/lib/src/auth/IAuthOptions';

export interface SharepointSite {
  authOptions: IAuthOptions;
  sharepointUrl: string;
  siteName: string;
  siteUrl: string;
  baseFolder: string;
  serverRelativeUrl?: string;
  formDigestValue?: string;
  formDigestExpirationTimestamp?: number;
}
