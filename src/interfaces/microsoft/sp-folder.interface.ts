/*
Microsoft object SP.Folder
https://learn.microsoft.com/en-us/previous-versions/office/sharepoint-visio/jj245697(v=office.15)
*/

export interface SpFolder {
  Files: object;
  ListItemAllFields: object;
  ParentFolder: object;
  Properties: object;
  Folders: object;
  ItemCount: number;
  Name: string;
  ServerRelativeUrl: string;
  WelcomePage: string;
  __metadata: object;
  StorageMetrics: object;
  Exists: boolean;
  IsWOPIEnabled: boolean;
  ProgID: null;
  TimeCreated: string;
  TimeLastModified: string;
  UniqueId: string;
}
