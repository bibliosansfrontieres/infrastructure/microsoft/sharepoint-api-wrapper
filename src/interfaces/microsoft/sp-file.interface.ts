/*
Microsoft object SP.File
https://learn.microsoft.com/en-us/previous-versions/office/sharepoint-visio/jj246089(v=office.15)
*/

export interface SpFile {
  __metadata: object;
  Author: object;
  CheckedOutByUser: object;
  EffectiveInformationRightsManagementSettings: object;
  InformationRightsManagementSettings: object;
  ListItemAllFields: object;
  LockedByUser: object;
  ModifiedBy: object;
  Properties: object;
  VersionEvents: object;
  Versions: object;
  CheckInComment: string;
  CheckOutType: number;
  ContentTag: string;
  CustomizedPageStatus: number;
  ETag: string;
  Exists: boolean;
  IrmEnabled: boolean;
  Length: string;
  Level: number;
  LinkingUri: string;
  LinkingUrl: string;
  MajorVersion: number;
  MinorVersion: number;
  Name: string;
  ServerRelativeUrl: string;
  TimeCreated: string;
  TimeLastModified: string;
  Title: string;
  UIVersion: number;
  UIVersionLabel: number;
  UniqueId: string;
}
