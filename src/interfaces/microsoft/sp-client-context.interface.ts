/*
Microsoft object SP.ClientContext
https://learn.microsoft.com/en-us/previous-versions/office/sharepoint-visio/jj245759(v=office.15)
*/

export interface SpClientContext {
  GetContextWebInformation: {
    __metadata: { type: 'SP.ContextWebInformation' };
    FormDigestTimeoutSeconds: number;
    FormDigestValue: string;
    LibraryVersion: string;
    SiteFullUrl: string;
    SupportedSchemaVersions: { __metadata: object; results: any[] };
    WebFullUrl: string;
  };
}
