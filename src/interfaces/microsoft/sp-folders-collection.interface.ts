/*
List of Microsoft object SP.Folder
*/
import { SpFolder } from './sp-folder.interface';

export interface SpFoldersCollection {
  results: SpFolder[];
}
