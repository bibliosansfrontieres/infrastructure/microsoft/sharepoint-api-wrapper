/*
List of Microsoft object SP.File
*/
import { SpFile } from './sp-file.interface';

export interface SpFilesCollection {
  results: SpFile[];
}
