import { IAuthOptions } from 'node-sp-auth/lib/src/auth/IAuthOptions';

export interface SharepointApiOptions {
  authOptions: IAuthOptions;
  url: string;
  siteName: string;
  baseFolder?: string;
}
