import * as spauth from 'node-sp-auth';
import { SpClientContext } from './interfaces/microsoft/sp-client-context.interface';
import { SpFile } from './interfaces/microsoft/sp-file.interface';
import { SpFilesCollection } from './interfaces/microsoft/sp-files-collection.interface';
import { SpFolder } from './interfaces/microsoft/sp-folder.interface';
import { SpFoldersCollection } from './interfaces/microsoft/sp-folders-collection.interface';
import { SpWeb } from './interfaces/microsoft/sp-web.interface';
import fetch, { Response } from 'node-fetch';
import { SharepointSite } from './interfaces/sharepoint-site.interface';
import { SharepointApiOptions } from './interfaces/sharepoint-api-options.interface';
import { SharepointApiMethod } from './interfaces/sharepoint-api-method.interface';
import dotenv from 'dotenv';
import * as fs from 'fs';
import { join } from 'path';
import axios from 'axios';

dotenv.config();

export { SharepointApiOptions };

export class SharepointApi {
  private sharepointSite: SharepointSite;

  constructor(sharepointApiOptions: SharepointApiOptions) {
    const authOptions = sharepointApiOptions.authOptions;
    const sharepointUrl = sharepointApiOptions.url;
    const siteName = sharepointApiOptions.siteName;
    const serverRelativeUrl = `/sites/${siteName}`;
    const siteUrl = `${sharepointUrl}${serverRelativeUrl}`;
    const baseFolder = sharepointApiOptions.baseFolder || '/';

    this.sharepointSite = {
      authOptions,
      sharepointUrl,
      siteName,
      siteUrl,
      baseFolder,
      serverRelativeUrl,
    };
  }

  private async getAuthHeaders(): Promise<{ [key: string]: any } | undefined> {
    try {
      const auth = await spauth.getAuth(
        `${this.sharepointSite.siteUrl}`,
        this.sharepointSite.authOptions,
      );
      return auth.headers;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  private async getFormDigestHeaders(
    endpoint: string,
    method: SharepointApiMethod,
  ): Promise<{ [key: string]: string }> {
    let formDigestHeaders: { [key: string]: any } = {};
    if (method === 'post' && !endpoint.includes('contextinfo')) {
      if (
        this.sharepointSite.formDigestValue === undefined ||
        this.sharepointSite.formDigestExpirationTimestamp === undefined
      ) {
        await this.getFormDigestValue();
      } else {
        if (Date.now() >= this.sharepointSite.formDigestExpirationTimestamp) {
          await this.getFormDigestValue();
        }
      }
      formDigestHeaders = {
        'X-RequestDigest': this.sharepointSite.formDigestValue,
      };
    }
    return formDigestHeaders;
  }

  private getBufferHeaders(data?: string | ArrayBuffer): {
    [key: string]: any;
  } {
    let bufferHeaders: { [key: string]: any } = {};
    if (data instanceof ArrayBuffer) {
      bufferHeaders = { 'Content-Length': data.byteLength };
    }
    return bufferHeaders;
  }

  private async getFormDigestValue() {
    const spClientContext = await this.getClientContext();
    if (!spClientContext) return;

    const formDigestValue =
      spClientContext.GetContextWebInformation.FormDigestValue;
    // Remove 30 seconds from timeout to keep it safe
    const formDigestTimeoutSeconds =
      spClientContext.GetContextWebInformation.FormDigestTimeoutSeconds - 30;
    const formDigestTimeoutMilliseconds = formDigestTimeoutSeconds * 1000;

    this.sharepointSite.formDigestValue = formDigestValue;

    const expiration = Date.now() + formDigestTimeoutMilliseconds;
    this.sharepointSite.formDigestExpirationTimestamp = expiration;
  }

  private getServerRelativeUrl(relativeUrl: string): string {
    // A relativeUrl must begin with a "/"
    if (!relativeUrl.startsWith('/')) {
      relativeUrl = `/${relativeUrl}`;
    }

    // Is baseFolder is included in relativeUrl ?
    if (!relativeUrl.includes(this.sharepointSite.serverRelativeUrl || '')) {
      let baseFolder = `/${this.sharepointSite.baseFolder}`;
      // Is serverRelativeUrl includes base folder (or root folder) ?
      if (relativeUrl.startsWith(this.sharepointSite.baseFolder)) {
        baseFolder = `/`;
      }
      if (relativeUrl.startsWith(`/${this.sharepointSite.baseFolder}`)) {
        baseFolder = '';
      }
      relativeUrl = `${this.sharepointSite.serverRelativeUrl}${baseFolder}${relativeUrl}`;
    }

    return relativeUrl;
  }

  private encodeServerRelativeUrl(serverRelativeUrl: string) {
    serverRelativeUrl = this.getServerRelativeUrl(serverRelativeUrl);
    serverRelativeUrl = serverRelativeUrl.replace(/'/g, '%27');
    serverRelativeUrl = encodeURI(serverRelativeUrl);
    return serverRelativeUrl;
  }

  private async fetchApi(
    url: string,
    method?: SharepointApiMethod,
    data?: string | ArrayBuffer,
    headers?: { [key: string]: string },
  ): Promise<Response | undefined> {
    method = method || 'get';

    const authHeaders = await this.getAuthHeaders();
    if (authHeaders === undefined) {
      console.error(
        'SharepointApiWrapper error : Could not connect to Sharepoint API',
      );
      return undefined;
    }

    const formDigestHeaders = await this.getFormDigestHeaders(url, method);

    const bufferHeaders = this.getBufferHeaders(data);

    headers = {
      ...headers,
      ...authHeaders,
      ...formDigestHeaders,
      ...bufferHeaders,
    };

    try {
      return fetch(`${url}`, {
        method: method || 'get',
        body: data,
        headers: {
          ...headers,
        },
      });
    } catch (e) {
      console.error('SharepointApiWrapper error : ', e, url);
      return undefined;
    }
  }

  private async jsonApi<T>(
    endpoint: string,
    method?: SharepointApiMethod,
    data?: string | ArrayBuffer,
  ): Promise<T | undefined> {
    const headers = {
      'Content-Type': 'application/json;odata=verbose',
      Accept: 'application/json;odata=verbose',
    };

    const res = await this.fetchApi(
      `${this.sharepointSite.siteUrl}/_api/${endpoint}`,
      method,
      data,
      headers,
    );
    if (!res) return undefined;
    const json = await res.json();
    if (!json) {
      console.error(
        'SharepointApiWrapper error: Could not parse to JSON',
        endpoint,
      );
      return undefined;
    }
    return json.d;
  }

  public async getClientContext() {
    const spClientContext = await this.jsonApi<SpClientContext | undefined>(
      'contextinfo',
      'post',
    );
    if (!spClientContext) return undefined;
    return spClientContext;
  }

  public async getSiteInfos(): Promise<SpWeb | undefined> {
    const spWeb = await this.jsonApi<SpWeb>('web');
    if (!spWeb) return undefined;
    return spWeb;
  }

  async getFolders(folderRelativeUrl = ''): Promise<SpFolder[] | undefined> {
    folderRelativeUrl = this.encodeServerRelativeUrl(folderRelativeUrl);

    const spFoldersCollection = await this.jsonApi<SpFoldersCollection>(
      `web/GetFolderByServerRelativeUrl('${folderRelativeUrl}')/Folders`,
    );

    if (!spFoldersCollection) return undefined;
    return spFoldersCollection.results;
  }

  async getFolder(folderRelativeUrl = ''): Promise<SpFolder | undefined> {
    folderRelativeUrl = this.encodeServerRelativeUrl(folderRelativeUrl);

    const spFolder = await this.jsonApi<SpFolder>(
      `web/GetFolderByServerRelativeUrl('${folderRelativeUrl}')`,
    );

    if (!spFolder) return undefined;
    return spFolder;
  }

  async moveFolder(
    folderRelativeUrl: string,
    toFolderRelativeUrl: string,
    newName?: string,
  ): Promise<void> {
    // Change older relativeUrl to new one
    const folderRelativeUrlSplit = folderRelativeUrl.split('/');
    const folder = folderRelativeUrlSplit[folderRelativeUrlSplit.length - 1];
    toFolderRelativeUrl = `${toFolderRelativeUrl}/${newName || folder}`;

    toFolderRelativeUrl = this.encodeServerRelativeUrl(toFolderRelativeUrl);
    folderRelativeUrl = this.encodeServerRelativeUrl(folderRelativeUrl);

    await this.jsonApi(
      `web/GetFolderByServerRelativeUrl('${folderRelativeUrl}')/MoveTo(newUrl='${toFolderRelativeUrl}')?@target='${this.sharepointSite.siteUrl}'`,
      'post',
    );
  }

  async getFiles(folderRelativeUrl = ''): Promise<SpFile[] | undefined> {
    folderRelativeUrl = this.encodeServerRelativeUrl(folderRelativeUrl);

    const spFilesCollection = await this.jsonApi<SpFilesCollection>(
      `web/GetFolderByServerRelativeUrl('${folderRelativeUrl}')/Files`,
    );

    if (!spFilesCollection) return undefined;
    return spFilesCollection.results;
  }

  async getFile(fileRelativeUrl: string): Promise<SpFile | undefined> {
    fileRelativeUrl = this.encodeServerRelativeUrl(fileRelativeUrl);

    const spFile = await this.jsonApi<SpFile>(
      `web/GetFileByServerRelativeUrl('${fileRelativeUrl}')`,
    );

    if (!spFile) return undefined;
    return spFile;
  }

  async getFileByUniqueId(fileUniqueId: string): Promise<SpFile | undefined> {
    const spFile = await this.jsonApi<SpFile>(
      `web/GetFileById('${fileUniqueId}')`,
    );

    if (!spFile) return undefined;
    return spFile;
  }

  async downloadFile(
    fileRelativeUrl: string,
    writeStream: fs.WriteStream,
    multiPartsOptions?: {
      contentLength?: number;
      chunkSize?: number;
    },
  ): Promise<boolean> {
    let isDownloaded = false;
    fileRelativeUrl = this.getServerRelativeUrl(fileRelativeUrl);
    fileRelativeUrl = fileRelativeUrl.replace(/'/g, '%27');

    if (!multiPartsOptions) {
      multiPartsOptions = {};
    }

    if (!multiPartsOptions.contentLength) {
      const spFile = await this.getFile(fileRelativeUrl);
      if (!spFile) return false;
      multiPartsOptions.contentLength = parseInt(spFile.Length);
    }

    if (!multiPartsOptions.chunkSize) {
      multiPartsOptions.chunkSize = 1024 * 1024;
    }

    const chunkSize = multiPartsOptions.chunkSize;
    const contentLength = multiPartsOptions.contentLength;

    for (let start: number = 0; start < contentLength; start += chunkSize) {
      const end: number = Math.min(start + chunkSize - 1, contentLength - 1);
      const headers = { Range: `bytes=${start}-${end}` };
      const res = await this.fetchApi(
        `${this.sharepointSite.siteUrl}/_layouts/15/download.aspx?sourceUrl=${fileRelativeUrl}`,
        undefined,
        undefined,
        headers,
      );

      if (!res || res.status !== 206) {
        isDownloaded = false;
        break;
      }

      await new Promise<void>((resolve) => {
        res.body.pipe(writeStream, { end: false });
        res.body.on('end', () => {
          resolve();
        });
      });

      if (start + chunkSize >= contentLength) {
        writeStream.end();
        if (fs.statSync(writeStream.path).size === contentLength) {
          isDownloaded = true;
          break;
        }
        isDownloaded = false;
        break;
      }
    }
    return isDownloaded;
  }

  async downloadFileById(
    fileUniqueId: string,
    writeStream: fs.WriteStream,
  ): Promise<boolean> {
    try {
      const fileUrl = `${this.sharepointSite.siteUrl}/_layouts/15/download.aspx?UniqueId=${fileUniqueId}`;
      
      // Fetch authentication headers
      const authHeaders = await this.getAuthHeaders();
      if (!authHeaders) {
        console.error('Error: Could not retrieve authentication headers.');
        return false;
      }
      
      // Fetch form digest headers if required (assuming getFormDigestHeaders is necessary)
      const formDigestHeaders = await this.getFormDigestHeaders(fileUrl, 'get');
  
      // Combine all headers
      const headers = {
        ...authHeaders,
        ...formDigestHeaders,
      };
  
      const response = await axios({
        url: fileUrl,
        method: 'GET',
        responseType: 'stream',
        headers: headers,
      });
  
      await new Promise<void>((resolve, reject) => {
        response.data.pipe(writeStream);
        response.data.on('end', () => resolve());
        response.data.on('error', (error: Error) => reject(error));
      });
  
      const fileStats = fs.statSync(writeStream.path);
      if (fileStats.size > 0) {
        return true;
      }
  
      return false;
    } catch (error) {
      console.error('Error downloading file:', error);
      return false;
    }
  }

  async uploadFile(
    fileName: string,
    file: Buffer,
    folderRelativeUrl = '',
  ): Promise<SpFile | undefined> {
    folderRelativeUrl = this.encodeServerRelativeUrl(folderRelativeUrl);

    const spFile = await this.jsonApi<SpFile>(
      `web/GetFolderByServerRelativeUrl('${folderRelativeUrl}')/Files/add(overwrite=true, url='${fileName}')?@target='${this.sharepointSite.siteUrl}'`,
      'post',
      file,
    );

    if (!spFile) return undefined;
    return spFile;
  }

  async moveFile(
    fileRelativeUrl: string,
    toFolderRelativeUrl: string,
  ): Promise<void> {
    const fileRelativeUrlSplit = fileRelativeUrl.split('/');
    const file = fileRelativeUrlSplit[fileRelativeUrlSplit.length - 1];
    toFolderRelativeUrl = `${toFolderRelativeUrl}/${file}`;

    fileRelativeUrl = this.encodeServerRelativeUrl(fileRelativeUrl);
    toFolderRelativeUrl = this.encodeServerRelativeUrl(toFolderRelativeUrl);

    await this.jsonApi(
      `web/GetFileByServerRelativeUrl('${fileRelativeUrl}')/MoveTo(newUrl='${toFolderRelativeUrl}',flags=1)?@target='${this.sharepointSite.siteUrl}'`,
      'post',
    );
  }

  async deleteFile(fileRelativeUrl: string): Promise<void> {
    fileRelativeUrl = this.encodeServerRelativeUrl(fileRelativeUrl);

    await this.jsonApi(
      `web/GetFileByServerRelativeUrl('${fileRelativeUrl}')/deleteObject`,
      'post',
    );
  }

  async createFolder(
    folderName: string,
    folderRelativeUrl?: string,
  ): Promise<SpFolder | undefined> {
    folderRelativeUrl = this.getServerRelativeUrl(folderRelativeUrl || '/');

    let separation = '';
    if (!folderRelativeUrl.endsWith('/') && !folderName.startsWith('/')) {
      separation = '/';
    }

    const newFolderRelativeUrl = `${folderRelativeUrl}${separation}${folderName}`;

    const data = {
      __metadata: {
        type: 'SP.Folder',
      },
      ServerRelativeUrl: newFolderRelativeUrl,
    };

    const newFolder = await this.jsonApi<SpFolder>(
      `web/folders`,
      'post',
      JSON.stringify(data),
    );

    return newFolder;
  }

  async deleteFolder(folderRelativeUrl: string): Promise<void> {
    folderRelativeUrl = this.encodeServerRelativeUrl(folderRelativeUrl);

    await this.jsonApi(
      `web/GetFolderByServerRelativeUrl('${folderRelativeUrl}')/deleteObject`,
      'post',
    );
  }
}
